package texed;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;
/**
 * 
 * @author Ruben Heintjens
 *this class contains all the buttons and the notification area
 */
public class ButtonPanel extends JPanel {
	
	private JButton undoButton, redoButton, complete, check;
	private JTextArea message;
	/**
	 * Initiates the panel and all buttons
	 * places the buttons in the layout
	 */
	public ButtonPanel() {
		Dimension size = getPreferredSize();
		size.width = 125;
		setPreferredSize(size);
		
		setBorder(BorderFactory.createTitledBorder("Controls"));
		
		undoButton = new JButton("undo");
		redoButton = new JButton("redo");
		complete = new JButton("Autocomplete");
		check = new JButton("Check");
		
		message = new JTextArea("Message");
		
		
		setLayout(new GridBagLayout());
		
		GridBagConstraints gc = new GridBagConstraints();
		
		gc.weightx = 0.5;
		gc.weighty = 0.5;
		gc.gridx = 0;
		gc.gridy = 0;
		add(undoButton, gc);
		
		gc.gridx = 0;
		gc.gridy = 1;
		add(redoButton, gc);
		
		gc.gridx = 0;
		gc.gridy = 2;
		add(complete, gc);
		
		gc.gridx = 0;
		gc.gridy = 3;
		add(check, gc);
		
		gc.gridx = 0;
		gc.gridy = 4;
		add(message, gc);
		
	}
	/*
	 * returns the undoButton
	 */
	public JButton getUndoButton() {
		return undoButton;
	}
	/*
	 * returns the redoButton
	 */
	public JButton getRedoButton() {
		return redoButton;
	}
	/*
	 * returns the completeButton
	 */
	public JButton getCompleteButton() {
		return complete;
	}
	/*
	 * returns the checkButton
	 */
	public JButton getCheckButton() {
		return check;
	}
	/*
	 * returns the notification area
	 */
	public JTextArea getMessage() {
		return message;
	}
	
}
