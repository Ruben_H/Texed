package texed;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import sun.security.util.Length;

/**
 * Simple GUI for a text editor.
 *Some comments within this class are simply used for debugging
 *
 */
public class Texed extends JFrame implements DocumentListener {
	private JTextArea textArea;
	
	private StackAdapter undoStack, redoStack, tagStack, globalTagStack;
	private ButtonPanel buttonPanel;

	private static final long serialVersionUID = 5514566716849599754L;
	/**
	 * Constructs a new GUI: A TextArea on a ScrollPane and also adds the buttonpanel
	 * defines the eventhandlers for the buttons
	 */
	public Texed() {
		super();
		setTitle("Texed: simple text editor");
		setBounds(900, 400, 600, 600);
		textArea = new JTextArea(30, 80);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		
		buttonPanel = new ButtonPanel();
		
		Container c = getContentPane();
		
		//Registration of the callback
		textArea.getDocument().addDocumentListener(this);
		
		JScrollPane scrollPane = new JScrollPane(textArea);
		setLayout(new BorderLayout());
		c.add(scrollPane, BorderLayout.CENTER);
		c.add(buttonPanel, BorderLayout.WEST);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		undoStack = new StackAdapter();
		redoStack = new StackAdapter();
		tagStack = new StackAdapter();
		globalTagStack = new StackAdapter();
		
		buttonPanel.getUndoButton().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Edits edit = (Edits) undoStack.pop();
				String s = textArea.getText();
				if (edit.getOffset()== s.length()-1) {
					System.out.println(textArea.getText().substring(s.length()-1)+" working at the end");
				if (edit.getAddition()) {
						textArea.setText(textArea.getText().substring(0, s.length()-edit.getLength()));
						StringBuffer str = new StringBuffer(textArea.getText().substring(edit.getOffset(), edit.getOffset()+ edit.getLength()+1));
						edit.setText(str.toString());
						/**
						 * register when a tag is removed in order to be able to re autocomplete
						 * if(edit.getText()== ">") {
							for (int i= edit.getOffset(); i>0; i--) {
								if (s.charAt(i) == '<') {
									if (s.charAt(i+1) == '/') {
										String tag = s.substring(i+1, edit.getOffset()+1);
										tagStack.push(tag);
									}
								}
							}
						}
						**/
				}
				else {
					textArea.append(edit.getText());
				}
				}
				else {
					if (edit.getAddition()) {
						String subB = s.substring(0, edit.getOffset());
						String subE = s.substring(edit.getOffset()+edit.getLength());
						textArea.setText(subB.concat(subE));
						StringBuffer str = new StringBuffer(textArea.getText().charAt(edit.getOffset()));
						edit.setText(str.toString());
					}
					else {
						StringBuilder str = new StringBuilder(s);
						System.out.println(edit.getText());
						str.setCharAt(edit.getOffset(), edit.getText().charAt(0));
						String string = str.toString();
						textArea.setText(string);
					}
				}
				edit.setAddition(!edit.getAddition());
				redoStack.push(edit);
			}
		});
		
		buttonPanel.getRedoButton().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Edits edit = (Edits) redoStack.pop();
				String s = textArea.getText();
				if (edit.getOffset()== s.length()-1) {
				if (edit.getAddition()) {
					textArea.setText(textArea.getText().substring(0, s.length()-2));
				}
				else {
					textArea.append(edit.getText());
				}
				}
				else {
					if (edit.getAddition()) {
						String subB = s.substring(0, edit.getOffset());
						String subE = s.substring(edit.getOffset()+1);
						textArea.setText(subB.concat(subE));
					}
					else {
						StringBuffer str = new StringBuffer(s);
						str.setCharAt(edit.getOffset(), edit.getText().charAt(0));
						String string = str.toString();
						textArea.setText(string);
					}
				}
				edit.setAddition(!edit.getAddition());
				undoStack.push(edit);
			}
			
		});
		
		buttonPanel.getCompleteButton().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				while(tagStack.isEmpty() == false) {
					System.out.println("complete");
					String temp = "</" + (String) tagStack.pop()+ ">";
					textArea.append(temp);
					Edits edit = new Edits(temp,true,textArea.getText().length()-1, 4);
					undoStack.push(edit);
				}
				
			}
			
		});
		
		buttonPanel.getCheckButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (tagStack.isEmpty() == false) {
					buttonPanel.getMessage().setText("tag not closed");
				}
				else {
					buttonPanel.getMessage().setText("tags are closed");
					
				}
				
			}
			
		});
		
		
	}

	/**
	 * Callback when changing an element
	 */
	public void changedUpdate(DocumentEvent ev) {
	}

	/**
	 * Callback when deleting an element
	 */
	public void removeUpdate(DocumentEvent ev) {
		//Check if the change is only a single character, otherwise return so it does not go in an infinite loop
				if(ev.getLength() != 1) return;
				
				// In the callback you cannot change UI elements, you need to start a new Runnable
				String temp = ev.toString();
				SwingUtilities.invokeLater(new Task( false, ev.getOffset(),ev));
				
	}

	/**
	 * Callback when inserting an element
	 */
	public void insertUpdate(DocumentEvent ev) {
		//Check if the change is only a single character, otherwise return so it does not go in an infinite loop
		if(ev.getLength() != 1) return;
		
		// In the callback you cannot change UI elements, you need to start a new Runnable
		//System.out.println("e");
		String temp = ev.toString();
		//used to be new task("",.....)
		SwingUtilities.invokeLater(new Task( true, ev.getOffset(), ev));
	}

	/**
	 * Runnable: change UI elements as a result of a callback
	 * Start a new Task by invoking it through SwingUtilities.invokeLater
	 */
	private class Task implements Runnable {
		Edits edit;
		DocumentEvent ev;
		boolean addition = true;
		private String text;
		
		
		/**
		 * Pass parameters in the Runnable constructor to pass data from the callback 
		 * @param 
		 * @param addition: whether the change is an addition or a removal
		 * @param ev: the DocumentEvent that called for the task
		 * 
		 */
		Task(boolean addition, int offset, DocumentEvent ev) {
			this.text = "";
			this.edit = new Edits(text, addition, offset);
			this.ev = ev;
			System.out.println(text +"testing");
		}

		/**
		 * The entry point of the runnable
		 */
		public void run() {
			textArea.append(text);
			
			undoStack.push(edit);
			opening(ev, textArea.getText(), text);
		}
		
		/**
		 * 
		 * @param ev: the event that called the task that called the runnable
		 * @param s: the string that is in the textArea
		 * @param text empty string (should be the change that has been made to the string)
		 * 
		 * this method registers when a tag has been added, when a tag has been closed and makes sure the order is correct 
		 * this method also auto-corrects order mistakes (not every mistake)
		 */
		public void opening(DocumentEvent ev, String s, String text) {
			String tag = null;
			int j =0;
			if (s.charAt(s.length()-1) == '>') {
				System.out.println("eindbracket gevonden");
				for (int i = s.length()-1; i >= 0; i--) {
					if(j==0) {
					if(s.charAt(i) == '<') {
						System.out.println("beginbracket gevonden");
						if (s.charAt(i+1) == '/') {
							tag = s.substring(i+2, s.length()-1);
							System.out.println("eind "+tag);
							endTagHandler(tag, i, s.length()-1);
							j++;
							
						}
						else {
							
							tag = s.substring(i+1, s.length()-1);
							beginTagHandler(tag);
							System.out.println("begin "+tag);
							j++;
						}
					}
					}
				}
			}
			
		}
		
		/**
		 * 
		 * @param tag the current tag being evaluated
		 * @param start beginning of the tag within the string of text
		 * @param eind end of the tag within the string of text
		 * this method handles registered endtags
		 */
		public void endTagHandler(String tag, int start, int eind) {
			if (tag == tagStack.top()) {
				//continue as usual
				System.out.println("normal");
			}
			else {
				
				//check tag order
				System.out.println("wrong order");
				textArea.setText(textArea.getText().substring(0, start).concat(textArea.getText().substring(eind, textArea.getText().length())));
				while (tagStack.isEmpty() == false) {
					if(tagStack.top() != tag) {
					System.out.println("ik ben in de lus");
					textArea.append("</"+tagStack.pop()+">");
					}
					else {
						System.out.println("laatste keer in de lus");
						textArea.append("</"+tagStack.pop()+">");
						
					}
				}
				
			}
		}
		
		/**
		 * 
		 * @param tag the tag being evaluated at the moment
		 * this method handles opening tags
		 */
		public void beginTagHandler(String tag){
			tagStack.push(tag);
			System.out.println(tagStack.top());
			System.out.println("begintagHandler");
		}
		
		/**
		 * 
		 * @param ev the current documentEvent
		 * this method corrects the mistake made when opening a new tag inbetween an already closed tag that the new tag is being closed outside of the existing tag
		 * example: <a><b></b></a> => <a><b> <c> </b></a> </c> instead of:<a><b> <c> </c></b></a> 
		 */
		public void inbetweenTags(DocumentEvent ev) {
			int index = 0;
			
		}
		
		
		
		
	}

	/**
	 * Entry point of the application: starts a GUI
	 */
	public static void main(String[] args) {
		new Texed();

	}

}