package texed;

import java.awt.TextArea;

import javax.swing.JTextArea;
/**
 * 
 * @author Ruben Heintjens
 *this class represents an edit of the string in the textArea
 */
public class Edits {
	
	private boolean addition;
	private String edit;
	private int length;
	private int location;
	
	/**
	 * 
	 * @param s the change made to the string in the textArea
	 * @param addition wheater the change is an addition or a deletion
	 * @param offset the offset of the change within the string
	 */
	public Edits(String s, boolean addition, int offset){
		this.addition = addition;
		this.edit = s;
		this.location = offset;
		this.length = 1;
	}
	/**
	 * 
	 * @param s the change made to the string in the textArea
	 * @param addition wheater the change is an addition or a deletion
	 * @param offset the offset of the change within the string
	 * @param length the length of the change
	 */
	public Edits(String s, boolean addition, int offset, int length){
		this.addition = addition;
		this.edit = s;
		this.location = offset;
		this.length = length;
	}
	
	/**
	 * 
	 * @return addition
	 */
	public boolean getAddition() {
		return addition;
	}
	/**
	 * 
	 * @return edit
	 */
	public String getText() {
		return edit;
	}
	/**
	 * 
	 * @return location (the offset)
	 */
	public int getOffset() {
		return location;
	}
	/**
	 * 
	 * @param bool
	 * sets the changetype to addition(true) or deletion(false)
	 */
	public void setAddition(boolean bool) {
		this.addition = bool;
	}
	/**
	 * 
	 * @param s
	 * changes edit
	 */
	public void setText(String s) {
		this.edit = s;
	}
	/**
	 * 
	 * @return the length of the change
	 */
	public int getLength() {
		return length;
	}
	

}
